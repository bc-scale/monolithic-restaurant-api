package payment

import (
	"gorm.io/gorm"
)

type IRepository interface {
	GetPayments(query GetPaymentsQuery) ([]Payment, error)
	AddPayment(request CreatePaymentRequest) (Payment, error)
	GetPaymentByID(paymentID int) (Payment, error)
	UpdatePayment(paymentID int, request UpdatePaymentRequest) (Payment, error)
	DeletePayment(paymentID int) error
	GetCookersHasMostReturn(limit int) ([]CookersReturn, error)
}
type Repository struct{
	db * gorm.DB
}
func NewRepository(db *gorm.DB) *Repository {
	return &Repository{db:db}
}
func (r * Repository) GetPayments(query GetPaymentsQuery) ([]Payment, error) {
	var payments []Payment
	err := r.db.Transaction(func(tx *gorm.DB) error {
		err := tx.Preload(PaymentTypeTable).Model(&Payment{}).Where(&Payment{
			PaymentType:   PaymentType{
				Name: query.PaymentType,
			},
		}).Find(&payments).Error
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return nil ,err
	}
	return payments, nil
}
func (r * Repository) AddPayment(request CreatePaymentRequest) (Payment, error) {
	payment := Payment{
		CustomerID:    request.CustomerID,
		OrderID:       request.OrderID,
		PaymentTypeID: request.PaymentTypeID,
	}
	err := r.db.Transaction(func(tx *gorm.DB) error {
		if err := tx.Create(&payment).Error ; err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return Payment{}, err
	}
	return payment, nil
}
func (r * Repository) GetPaymentByID(paymentID int) (Payment, error) {
	var payment Payment
	err := r.db.Transaction(func(tx *gorm.DB) error {
		if err := tx.Preload(PaymentTypeTable).Where(&Payment{ID:paymentID}).Find(&payment).Error; err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return Payment{}, err
	}
	return payment, nil
}
func (r * Repository) UpdatePayment(paymentID int, request UpdatePaymentRequest) (Payment, error) {
	payment := Payment{
		ID:            paymentID,
		CustomerID:    request.CustomerID,
		OrderID:       request.OrderID,
		PaymentTypeID: request.PaymentTypeID,
	}
	err := r.db.Transaction(func(tx *gorm.DB) error {
		if err:= tx.Save(&payment).Error ; err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return Payment{}, err
	}
	return payment, nil
}
func (r * Repository) DeletePayment(paymentID int) error {
	err := r.db.Transaction(func(tx *gorm.DB) error {
		if err := tx.Delete(&Payment{ID:paymentID}).Error; err != nil {
			return err
		}
		return nil
	})
	return err
}
func (r * Repository) GetCookersHasMostReturn(limit int) ([]CookersReturn, error) {
	var cookersReturns []CookersReturn
	err := r.db.Transaction(func(tx *gorm.DB) error {
		//err := tx.Table(PaymentTable).Select("Cooker.cooker_name, Food.price").
			//Joins(fmt.Sprintf("left join %s on Order.order_id = Payments.order_id","Order")).
			//	Joins("join Food on Food.food_id = Order.food_id").
			//		Joins("join Cooker on Cooker.cooker_id = Order.order_id").
			//			Group("Cooker.cooker_id").
			//				Limit(limit).Scan(&cookersReturns).Error
		//type mb struct {
		//	customerName string
		//}
		//err := tx.Model(&Payment{}).Select("")
		//if err != nil {
		//	return err
		//}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return cookersReturns, nil
}
