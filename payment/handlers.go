package payment

import "github.com/gofiber/fiber/v2"

type Handlers struct {
	service IService
}
func NewHandlers(service IService) *Handlers {
	return &Handlers{service: service}
}
func (h *Handlers) GetPayments(c *fiber.Ctx) error {
	var query GetPaymentsQuery
	if err:= c.QueryParser(&query); err != nil {
		return c.SendStatus(fiber.StatusBadRequest)
	}
	payments, err := h.service.GetPayments(query)
	if err != nil {
		return c.SendStatus(fiber.StatusExpectationFailed)
	}
	return c.Status(fiber.StatusOK).JSON(payments)
}
func (h *Handlers) GetPaymentByID(c *fiber.Ctx) error {
	paymentID,err := c.ParamsInt("id")
	if err != nil {
		return c.SendStatus(fiber.StatusBadRequest)
	}
	var payment Payment
	if payment,err = h.service.GetPaymentByID(paymentID) ; err != nil {
		return c.SendStatus(fiber.StatusExpectationFailed)
	}
	return c.Status(fiber.StatusOK).JSON(payment)
}
func (h *Handlers) UpdatePayment(c *fiber.Ctx) error {
	paymentID,err := c.ParamsInt("id")
	if err != nil {
		return c.SendStatus(fiber.StatusBadRequest)
	}
	var updateRequest UpdatePaymentRequest
	if err = c.BodyParser(&updateRequest); err != nil {
		return c.SendStatus(fiber.StatusBadRequest)
	}
	var payment Payment
	if payment, err = h.service.UpdatePayment(paymentID,updateRequest); err != nil {
		return c.SendStatus(fiber.StatusExpectationFailed)
	}
	return c.Status(fiber.StatusOK).JSON(payment)
}
func (h *Handlers) DeletePayment(c *fiber.Ctx) error {
	paymentID,err := c.ParamsInt("id")
	if err != nil {
		return c.SendStatus(fiber.StatusBadRequest)
	}
	if err = h.service.DeletePayment(paymentID); err != nil {
		return c.SendStatus(fiber.StatusExpectationFailed)
	}
	return c.SendStatus(fiber.StatusOK)
}
func (h *Handlers) RegisterRoutes(app *fiber.App) {
	app.Get("/payments",h.GetPayments)
	app.Get("/payments/:id",h.GetPaymentByID)
	app.Put("/payments/:id",h.UpdatePayment)
	app.Delete("/payments/:id",h.DeletePayment)
}