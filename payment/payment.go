package payment

import "restaurant-api/postgre"

type Payment struct {
	ID int `gorm:"primaryKey;column:payment_id" json:"paymentID"`
	CustomerID int `gorm:"column:customer_id" json:"customerID"`
	OrderID int `gorm:"column:order_id" json:"order_id"`
	PaymentTypeID int `gorm:"column:payment_type_id" json:"paymentTypeID"`
	PaymentType PaymentType `gorm:"foreignKey:PaymentTypeID" json:"paymentType"`
}
func (Payment) TableName() string {
	return postgre.CreateTableName(TablePrefix, PaymentTable)
}
func (Payment) JoinTableName(joinTable string) string {
	return postgre.CreateTableName(TablePrefix, joinTable)
}
type PaymentType struct {
	ID int `gorm:"primaryKey;column:payment_type_id" json:"paymentTypeID"`
	Name string `gorm:"column:payment_type_name" json:"paymentTypeName"`
}
func (PaymentType) TableName() string {
	return postgre.CreateTableName(TablePrefix, PaymentTypeTable)
}
type CookersReturn struct {
	Name string `json:"cookerName"`
	Return int `json:"cookerReturn"`
}
const (
	TablePrefix = "restaurant"
	PaymentTable = "Payment"
	PaymentTypeTable = "PaymentType"
)