package order

type CreateOrderRequest struct {
	CustomerID int `json:"customerID"`
	FoodID int `json:"foodID"`
	CookerID int `json:"cookerID"`
}
type UpdateOrderFoodIdRequest struct {
	FoodID int `json:"foodID"`
}