// +build test

package tests

import (
	"context"
	"database/sql"
	"github.com/stretchr/testify/assert"
	"log"
	"restaurant-api/config"
	"restaurant-api/foods"
	"restaurant-api/postgre"
	"testing"
)


func createDbConfig() config.DB {
	return config.DB{
		Driver:   "postgres",
		Username: "postgres",
		Password: "mypassword",
		Host:     "localhost",
		Port:     "5432",
		DBName:   "postgres",
		TablePrefix: "restaurant.",
	}
}
func initializeRepository() *foods.Repository {
	config := createDbConfig()
	db, err := postgre.New(config)
	if err != nil {
		log.Println("Error initializing database")
		return nil
	}
	return foods.NewRepository(db)
}
func TestIGetAllTheFoodsFromDBWithFoodIDSuccessfully(t *testing.T) {
	repo := initializeRepository()
	ctx := context.Background()
	foods, err := repo.GetFoods(ctx,sql.TxOptions{})
	if err != nil {
		log.Println(err.Error())
	}
	for _,food := range foods {
		log.Println(food.Name)
	}
	assert.GreaterOrEqual(t, len(foods),1,"Not Found")
}
func TestIGetAddedFoodToDBSuccessfullyWithGivenParameters(t *testing.T) {
	repo := initializeRepository()
	type TestParameters struct {
		request foods.CreateFoodRequest
		context context.Context
		options sql.TxOptions
	}
	testParams := TestParameters{
		request: foods.CreateFoodRequest{
			FoodName:   "Chicken Fajita",
			FoodPrice:  50,
			CategoryID: 2,
		},
		context:    context.Background(),
		options:    sql.TxOptions{},
	}
	addedFood, err := repo.AddFood(testParams.context,testParams.options, testParams.request)
	if err != nil {
		log.Printf("\nError happening on adding foods to the repo %s", err.Error())
	}
	log.Printf("\n Added food: %v", addedFood)
	assert.Equal(t, addedFood.Name, testParams.request.FoodName,"Testing is the passed food name and added food name are equal")
}
func TestIGetUpdatedFoodFromDBWithGivenUpdateFoodRequest(t *testing.T) {
	repo := initializeRepository()
	foodToUpdateAs := foods.UpdateFoodRequest{
		FoodID:         7,
		FoodName:       "Chicken Burger",
		FoodPrice:      0,
		CategoryID:	2,
	}
	food, err := repo.UpdateFood(context.Background(),sql.TxOptions{},foodToUpdateAs)
	if err != nil {
		log.Printf("\nError happened: %s", err.Error())
	}
	assert.Equal(t, food.ID, foodToUpdateAs.FoodID,"Updated food has still same id")
	assert.Equal(t, foodToUpdateAs.FoodName, food.Name ,"Food name updated successfully.")
}
func TestGetFoodByIDSuccessfullyWithGivenFoodID(t *testing.T) {
	repo := initializeRepository()
	foodID := 7
	food, err := repo.GetFoodByID(context.Background(), sql.TxOptions{}, foodID)
	if err != nil {
		log.Printf("Error occured %s",err.Error())
	}
	log.Printf("Found food: %v", food)
	assert.Equal(t, foodID, food.ID,"Food successfully found from database")
}
func TestDeleteFoodFromDbByFoodIdSuccessfully(t *testing.T) {
	repo := initializeRepository()
	foodID:= 9
	err := repo.DeleteFood(context.Background(), sql.TxOptions{},foodID)
	if err != nil {
		log.Printf("Error occured when deleting food %s", err.Error())
	}
	assert.Equal(t, nil, err, "Delete food from db successfully")
}