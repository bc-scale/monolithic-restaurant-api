package foods

import "restaurant-api/postgre"

type Food struct {
	ID int `json:"foodID" gorm:"primaryKey;column:food_id"`
	Name string `json:"foodName" gorm:"column:food_name"`
	Price int `json:"foodPrice" gorm:"column:food_price"`
	CategoryID int `json:"categoryID" gorm:"column:category_id"`
	Category Category `json:"category" gorm:"foreignKey:CategoryID"`
}

/*TableName function provides food to belong Tabler interface of gorm. But the dangerous point on using this,
if you use custom schema table prefix on your db, that overrides it. So if you want to give a specific name to
your model but also do not want to lose the scheme name prefix, you must add it manually here.
*/
func (Food) TableName() string{
	return postgre.CreateTableName(TABLE_PREFIX,FOOD)
}
type Category struct {
	ID int `json:"categoryID" gorm:"primaryKey;column:category_id"`
	Name string `json:"categoryName" gorm:"column:category_name"`
}
func (Category) TableName() string{
	return postgre.CreateTableName(TABLE_PREFIX, FOOD_CATEGORY)
}
const(
	FOOD = "Food"
	//if you want to use foodcategory as preload(FOOD_CATEGORY) then, you need to use CATEGORY instead of it.Gorm works with the name of the struct
	//instead of name of the table on db which is FoodCategory in db and Category in here.
	FOOD_CATEGORY = "FoodCategory"
	CATEGORY = "Category"
	TABLE_PREFIX = "restaurant"
)