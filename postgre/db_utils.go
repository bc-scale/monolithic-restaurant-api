package postgre

import "fmt"

func CreateTableName(tablePrefix,tableName string) string {
	if tablePrefix != "" {
		return fmt.Sprintf("%s.%s",tablePrefix,tableName)
	}
	return tableName
}
